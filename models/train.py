import os

import numpy as np
import torch
from ignite.engine import Events, Engine, create_supervised_evaluator
from ignite.metrics import Loss
from models.confusion_matrix import ConfusionMatrix
from tqdm.autonotebook import tqdm
from . import loss as lossmodulecustom


def output_transform(x, y, y_pred):
    invalids = y <= 0
    y[invalids] = 1
    y -= 1
    return torch.softmax(y_pred, dim=1), y


def make_training_engine(model, optimizer, calc_loss, device):
    """
    Makes an ignite engine for training, adds
    support for channel weighting
    """
    def update(engine, batch):
        model.train()
        optimizer.zero_grad()
        x, y = batch
        invalids = y <= 0
        y[invalids] = 1
        y -= 1

        y_pred = model(x.to(device))
        loss = calc_loss(y_pred, y.to(device))
        loss.backward()
        optimizer.step()

        return loss.item()

    return Engine(update)


def train_model(model,
                dataloaders,
                optimizer,
                scheduler,
                calc_loss,
                device,
                output=True,
                pbartrain=True,
                num_classes=3,
                output_folder="./",
                prefix="unet",
                num_epochs=25,
                log_interval=1,
                multigpu=False):

    model_base = model
    # Parallel?
    if multigpu and device.type.startswith('cuda') and torch.cuda.device_count() > 1:
        print("Using ", torch.cuda.device_count(), " GPUs.")
        model = torch.nn.DataParallel(model)
    else:
        model = model

    train_loss, val_loss, cm_matrix_iter = [], [], []

    cross_entropy = torch.nn.CrossEntropyLoss()
    dice_loss = lossmodulecustom.dice_loss

    trainer = make_training_engine(model, optimizer, calc_loss, device=device)

    cm_object = ConfusionMatrix(num_classes=num_classes,
                                device=device)

    #    evaluation_loss = lambda y, y_pred : calc_loss(y, y_pred, model.channel_weights.cpu())
    #else:
    #    evaluation_loss = lambda y, y_pred : calc_loss(y, y_pred, None)

    evaluator = create_supervised_evaluator(model,
                                            metrics={'cm': cm_object,
                                                     'loss': Loss(calc_loss),
                                                     "dice_loss": Loss(dice_loss),
                                                     'cross_entropy': Loss(cross_entropy)},
                                            output_transform=output_transform,
                                            device=device)

    train_loader, val_loader = dataloaders['train'], dataloaders['val']

    desc = "Iteration - loss: {:.2f}"
    if pbartrain:
        pbar = tqdm(
            initial=0, leave=False, total=len(train_loader),
            desc=desc.format(0)
        )
    else:
        pbar = None

    torch.save(model_base.state_dict(), os.path.join(output_folder,
                                                        "{}_0_weights.pt".format(prefix)))

    if output:
        """
        Save checkpoints and do validation
        """
        @trainer.on(Events.EPOCH_COMPLETED)
        def log_validation_results(engine):
            print("Running validation on checkpoint")
            evaluator.run(val_loader)
            metrics = evaluator.state.metrics
            cm = metrics["cm"]
            avg_accuracy = cm.diag().sum() / (cm.sum() + 1e-15)
            tqdm.write("Validation Results - Epoch: {} | Avg acc: {:.2f} | Avg loss: {:.2f}"
                    .format(engine.state.epoch, avg_accuracy, metrics['loss'])
            )

            cm_matrix_iter.append(cm.tolist())
            val_loss.append((engine.state.iteration, avg_accuracy, metrics['loss']))

            if pbar is not None:
                pbar.n = pbar.last_print_n = 0

        @trainer.on(Events.EPOCH_COMPLETED)
        def save_epoch_model(engine):
            print("Saving checkpoint")
            torch.save(model_base.state_dict(),
                        os.path.join(output_folder,
                                    "{}_{}_weights.pt".format(prefix, engine.state.epoch + 1)))
            np.savetxt(os.path.join(output_folder,
                                    "{}_train_curve.txt".format(prefix)), np.array(train_loss))
            np.savetxt(os.path.join(output_folder,
                                    "{}_val_curve.txt".format(prefix)), np.array(val_loss))

            cm_matrix = np.array(cm_matrix_iter)
            np.savetxt(os.path.join(output_folder,
                                    "{}_val_confusion_matrix.txt".format(prefix)),
                        cm_matrix.reshape((cm_matrix.shape[0],
                                            np.prod(cm_matrix.shape[1:], dtype=np.int))))

    @trainer.on(Events.EPOCH_COMPLETED)
    def update_lr(engine):
        print("Finished epoch %d/%d" % (engine.state.epoch, num_epochs))
        scheduler.step()
        for param_group in optimizer.param_groups:
            print("Learning rate", param_group['lr'])

    @trainer.on(Events.COMPLETED)
    def save_final_model(engine):
        print("Saving final checkpoint")
        np.savetxt(os.path.join(output_folder,
                                    "{}_train_curve.txt".format(prefix)), np.array(train_loss))
        np.savetxt(os.path.join(output_folder,
                                "{}_val_curve.txt".format(prefix)), np.array(val_loss))
        torch.save(model_base.state_dict(), os.path.join(output_folder,
                                                         "{}_final_weights.pt".format(prefix)))

    @trainer.on(Events.ITERATION_COMPLETED)
    def log_training_loss(engine):
        iter = (engine.state.iteration - 1) % len(train_loader) + 1

        train_loss.append((engine.state.iteration, engine.state.output))

        if ((iter % log_interval) == 0) and (pbar is not None):
            pbar.desc = desc.format(engine.state.output)
            pbar.update(log_interval)

    trainer.run(train_loader, max_epochs=num_epochs)

    if pbar is not None:
        pbar.close()

    return
