import argparse
import pprint
import traceback
import sys
import os
from glob import glob
import time
import random
import numpy as np
from models import ndwi_test
import shutil
import torch
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.optim import lr_scheduler
import json
from natsort import natsorted
from models.architectures.baselines import SimpleLinear, SimpleCNN
from models.architectures.unets import UNet
from models.architectures.ndwi import ManualWaterBodyNDWI
from models.train import train_model
from models.loss import calc_loss
from models.test import test_loop, test_loop_thresholds
from data import worldfloods_dataset, worldfloods_dataloaders
from termcolor import colored
from utils import plot_preds, fsutils, pred_by_tiles, process_results


def set_random_seed(seed=None):
    if seed is None:
        seed = int((time.time()*1e6) % 1e8)
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)


def handle_dataset(opt, cv_index=None, subsets=["train", "val", "test"], normalize=True):
    num_class = 3

    cols = worldfloods_dataset.COLORS_WORLDFLOODS
    observed_frequencies = torch.Tensor(worldfloods_dataset.FREQUENCY_CLASSES)
    weight = 1 / observed_frequencies

    datasets = {}
    for subset in subsets:
        datasets[subset] = worldfloods_dataloaders.get_worldfloods_data(
            width=opt.window_size, subset=subset,
            downsampling_factor=opt.downsampling_factor, normalize=normalize,
            augment=opt.augment, worldfloods_root=opt.worldfloods_root,
            dataset_limit=opt.dataset_limit,
            use_channels=opt.s2_channels,
            cv_index=cv_index,
            folder_cache=opt.folder_cache)

    means, stds = worldfloods_dataloaders.get_normalisation(opt.s2_channels)
    num_channels = means.shape[2]

    print('Channels: {}'.format(num_channels))
    print('Output classes: {}'.format(num_class))

    batch_size = opt.batch_size

    # Dataloaders
    dataloaders = {}
    for k, d in datasets.items():
        dataloaders[k] = DataLoader(d, batch_size=batch_size if d.window_size is not None else 1,
                                    shuffle=k == "train", num_workers=opt.num_workers)

    return dataloaders, num_class, num_channels, cols, weight, means, stds


def handle_device(opt):
    if opt.device.startswith('cuda'):
        if not torch.cuda.is_available():
            opt.device = 'cpu'
            raise RuntimeError('CUDA is not available. use --device cpu')
        for c in range(torch.cuda.device_count()):
            print("Using device %s" % torch.cuda.get_device_name(c))
    return torch.device(opt.device)


def load_model_weights(model, model_folder, mode_train=True):
    files = natsorted(glob(os.path.join(model_folder, '*_weights.pt')))
    if len(files) == 0:
        if mode_train:
            return None
        else:
            raise FileNotFoundError(f"Model weights not found in folder {model_folder}")
    # print('Model files       : {}'.format(files))
    model_file = files[-1]
    print('Using latest model: {}'.format(model_file))
    model.load_state_dict(torch.load(model_file, map_location=lambda storage, loc: storage))
    return model_file


SUBSAMPLE_MODULE = {
    "unet": 8
}

def create_model(model_name, num_class, num_channels):
    if model_name is None:
        print('Expecting a --model argument')
        quit()
    if model_name == "linear":
        model = SimpleLinear(num_channels, num_class)
    elif model_name == "unet":
        model = UNet(num_channels, num_class)
    elif model_name == "simplecnn":
        model = SimpleCNN(num_channels, num_class)
    else:
        raise ModuleNotFoundError("model {} not found".format(model_name))

    print('Model  : {}'.format(model_name))
    return model


def trainleaveoneout(opt):
    model_folder_or = opt.model_folder
    for cv_index in range(len(worldfloods_dataloaders.CV_TEST_EXCLUDE)):
        print(colored("%d/%d Cross validation" % (cv_index, len(worldfloods_dataloaders.CV_TEST_EXCLUDE)),
                      'white', attrs=['bold']))
        opt.model_folder = model_folder_or+"_%02d"%cv_index
        last_weights_file = os.path.join(opt.model_folder, "{}_final_weights.pt".format(opt.model))
        if os.path.exists(last_weights_file):
            print("params exists for cv %02d continue" % cv_index)
            continue
        elif os.path.exists(os.path.join(opt.model_folder, "params.txt")):
            shutil.rmtree(opt.model_folder)

        train(opt, cv_index, dataset_eval="test")


def train(opt, cv_index=None, dataset_eval="val"):
    print(colored('Training mode', 'white', attrs=['bold']))

    # Load Dataset
    print('Dataset: WORLDFLOODS')
    subsets_dl = ["train", dataset_eval]
    dataloaders, num_class, num_channels, colors_cmap, weights, means, stds = handle_dataset(opt, cv_index=cv_index,
                                                                                             subsets=subsets_dl)
    print("Train length: ", len(dataloaders['train'].dataset))
    print("%s length: %d" % (dataset_eval, len(dataloaders[dataset_eval].dataset)))

    # Create dirs to save model stuff
    os.makedirs(opt.model_folder, exist_ok=True)
    params_path = os.path.join(opt.model_folder, "params.txt")

    # Store parameters
    if os.path.isfile(params_path) and not opt.overwrite:
        raise OSError("Parameter file {} already exists".format(params_path))
    else:
        with open(params_path, "w") as file:
            file.write(json.dumps(vars(opt)))

    # Choose device
    device = handle_device(opt)

    model = create_model(opt.model, num_class, num_channels)

    latest_weight_file = load_model_weights(model, opt.model_folder, mode_train=True)
    if latest_weight_file is None:
        print('Training model {} from scratch'.format(opt.model))
    else:
        print('Continuing to train existing model {} from checkpoint {}.'.format(opt.model,
                                                                                 latest_weight_file,
                                                                                 ))
    if device.type.startswith('cuda'):
        model = model.to(device)

    optimizer_ft = optim.Adam(model.parameters(), lr=opt.lr)
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=25, gamma=0.1)

    # Build loss function
    weight_classes = weights.to(device) if opt.weightloss else None
    calc_loss_func = lambda pred, target: calc_loss(pred, target,
                                                    bce_weight=opt.bce_weight,
                                                    filter_weight=None,
                                                    weight=weight_classes)

    prefix = (opt.model if opt.prefix is None else opt.prefix)

    train_model(model,
                [dataloaders[s] for s in subsets_dl],
                optimizer_ft,
                exp_lr_scheduler,
                calc_loss_func,
                device,
                num_classes=num_class,
                output=opt.output,
                pbartrain=opt.pbartrain,
                prefix=prefix,
                output_folder=opt.model_folder,
                num_epochs=opt.epochs,
                multigpu=opt.multigpu)

    print("Eval model in %s dataset" % dataset_eval)
    module_shape = SUBSAMPLE_MODULE[opt.model] if opt.model in SUBSAMPLE_MODULE else 1
    pred_fun_final = get_pred_function(model, device=device,
                                       module_shape=module_shape, max_tile_size=opt.max_tile_size)

    batch_size_val = 1 if dataset_eval == "test" else opt.batch_size
    with torch.no_grad():
        confusions = test_loop(dataloaders[dataset_eval], pred_fun_final, num_class, batch_size=batch_size_val)
    confusions = np.array(confusions)  # len(dataloaders['val'].dataset) x n_classes x n_classes matrix
    # confusion_total = np.sum(confusions, axis=0)  # n_classes x n_classes matrix

    filename_results = os.path.join(opt.model_folder, "metrics_worldfloods.json")

    latest_weight_file = os.path.join(opt.model_folder,
                                      "{}_final_weights.pt".format(prefix))

    extra = {"model_file": latest_weight_file,
             "model_folder": opt.model_folder,
             "model": opt.model}
             
    process_results.save_results(confusions, filename_results, num_class=num_class,
                                 extra=extra, verbose=opt.output)
    return


def get_pred_function(model, device=torch.device("cuda:0"), module_shape=1, max_tile_size=1280):
    if device.type.startswith('cuda'):
        model = model.to(device)
    model.eval()
    if module_shape > 1:
        pred_fun = pred_by_tiles.padded_predict(lambda ti: torch.softmax(model(ti.to(device)), dim=1),
                                                module_shape=module_shape)
    else:
        pred_fun = lambda ti: torch.softmax(model(ti.to(device)), dim=1)

    def pred_fun_final(ti, gt):
        if any((s > max_tile_size for s in ti.shape[2:])):
            return pred_by_tiles.predbytiles(pred_fun,
                                             input_batch=ti,
                                             tile_size=max_tile_size)
        return pred_fun(ti)

    return pred_fun_final


@torch.no_grad()
def precision_recall(opt):
    print(colored('Precision-Recall mode', 'white', attrs=['bold']))
    # Load Dataset
    print('Dataset: WORLDFLOODS')

    normalize_inputs = opt.model in {"linear", "simplecnn", "unet"}
    dataloaders, num_class, num_channels, colors_cmap, _, _, _ = handle_dataset(opt, subsets=[opt.dataset_test],
                                                                                normalize=normalize_inputs)
    print("Using dataset %s for test" % opt.dataset_test)
    dataloader = dataloaders[opt.dataset_test]
    print("Length: ", len(dataloader.dataset))

    fsutils.create_path(opt.test_folder, directory=True)
    fn_results = os.path.join(opt.test_folder, "pr_worldfloods.json")

    if opt.model == "ndwi":
        return ndwi_test.run_pr_curve_ndwi(fn_results,
                                           dataloader=dataloader,
                                           thresholds_water=np.arange(-1, 1.01, .02)[-1::-1])

    # Choose device
    device = handle_device(opt)

    # Create pred function
    model = create_model(opt.model, num_class, num_channels)
    latest_weight_file = load_model_weights(model, opt.model_folder, mode_train=False)

    if opt.model in SUBSAMPLE_MODULE:
        module_shape = SUBSAMPLE_MODULE[opt.model]
    else:
        module_shape = 1

    pred_fun_final = get_pred_function(model, device=device,
                                       module_shape=module_shape, max_tile_size=opt.max_tile_size)

    thresholds_water = [0, 0.001, 0.01] + np.arange(0.05, .95, 0.05).tolist() + [.95, .99, .999]

    confusions, thresholds_water = test_loop_thresholds(dataloader, pred_fun_final,
                                                        thresholds_water=np.array(thresholds_water))

    with open(fn_results, "w") as fh:
        json.dump({"confusion_matrices": [fsutils.castlistmatrix(x) for x in confusions],
                   "thresholds_water": fsutils.cast2(thresholds_water, float),
                  "latest_weight_file": latest_weight_file},
                  fh)


@torch.no_grad()
def test(opt):
    print(colored('Test mode', 'white', attrs=['bold']))
    # Load Dataset
    print('Dataset: WORLDFLOODS')
    normalize_inputs = opt.model in {"linear", "simplecnn", "unet"}
    dataloaders, num_class, num_channels, colors_cmap, _, means, stds = handle_dataset(opt, subsets=[opt.dataset_test],
                                                                                       normalize=normalize_inputs)
    print("Using dataset %s for test" % opt.dataset_test)
    dataloader = dataloaders[opt.dataset_test]
    print("Length: ", len(dataloader.dataset))

    if opt.model == "ndwi":
        pred_fun_final = ManualWaterBodyNDWI(threshold_water=opt.threshold_ndwi, to_probability=True)
        latest_weight_file = None
    else:
        model = create_model(opt.model, num_class, num_channels)
        module_shape = SUBSAMPLE_MODULE[opt.model] if opt.model in SUBSAMPLE_MODULE else 1
        pred_fun_final = get_pred_function(model, device=handle_device(opt),
                                           module_shape=module_shape, max_tile_size=opt.max_tile_size)
        latest_weight_file = load_model_weights(model, opt.model_folder, mode_train=False)

    fsutils.create_path(opt.test_folder, directory=True)

    idx_show = set(range(len(dataloader.dataset)))

    filename_fun = lambda _idx: os.path.join(opt.test_folder, 'worldfloods_test_{}.jpg'.format(_idx))
    batch_size = 1 if opt.dataset_test == "test" else opt.batch_size
    confusions = test_loop(dataloader, pred_fun_final, num_class, idx_show, batch_size,
                           kwargs_plot={"plot_probabilities_and_channels": opt.plotall,
                                        "normalized": normalize_inputs,
                                        "figsize": None,
                                        "colors_cmap": colors_cmap},
                           filename_fun=filename_fun)

    name_html = os.path.join(opt.test_folder, 'worldfloods_test.html')
    plot_preds.generate_html(list(idx_show), dataloader.dataset,
                             [filename_fun(idx) for idx in idx_show],
                             name_html)

    cm = np.array(confusions)
    extra = {"model_file": latest_weight_file,
             "model_folder": opt.model_folder,
             "model": opt.model}

    fn_results = os.path.join(opt.test_folder, "metrics_worldfloods.json")

    process_results.save_results(cm, fn_results, num_class=num_class,
                                 extra=extra, verbose=True)


def cache_data(args):
    """
    Creates a hdf5 file with the train and validation data.
    It chops all (training and val) tif files in tiles of window_size size

    :param args:
    :return:
    """
    from data import worldfloods_dataset_cache
    compression = args.compression if args.compression != "" else None

    if args.folder_cache is None:
        folder_cache = os.path.join(args.worldfloods_root, "caches")
    else:
        folder_cache = args.folder_cache

    for mode in ["train", "val"]:
        print("Generate %s cache in %s" % (mode, folder_cache))
        x_filenames = glob(os.path.join(args.worldfloods_root, mode, "S2", "*.tif"))
        filename_train = os.path.join(folder_cache,
                                      worldfloods_dataset_cache.filename_cache(mode, args.window_size))
        worldfloods_dataset_cache.cache_data((args.window_size, args.window_size),
                                             x_filenames=x_filenames, name_file_out=filename_train,
                                             compression=compression)


def parse_args(args=None, require_mode=True):
    parser = argparse.ArgumentParser(description='FDL Europe 2019 Disaster Management')
    parser.add_argument('--mode', help='Main mode', choices=['train', 'test',
                                                             "trainleaveoneout", "cache",
                                                             "pr"],
                        nargs='?', type=str, required=require_mode)
    parser.add_argument('--model', help='Model', choices=['ndwi', 'linear', 'simplecnn', 'unet'],
                       required=False, type=str)
    parser.add_argument('--output', dest="output", help="Produce output in training mode (only final weights)",
                        action="store_true")
    parser.add_argument('--no_output', dest="output", help="Produce no output in training mode (only final weights)",
                        action="store_false")
    parser.set_defaults(output=True)
    parser.add_argument('--worldfloods_root', help='Root folder for the WORLDFLOODS dataset', type=str,
                        default='/mnt/WORLDFLOODS/')
    parser.add_argument('--device', help='Device for training and testing', type=str, default='cuda:0')
    parser.add_argument('--batch_size', help='Batch size', type=int, default=64)
    parser.add_argument('--num_workers', help='Number of workers for data loaders', type=int, default=4)
    parser.add_argument('--model_folder', help='Folder to save model checkpoints during training', type=str)
    parser.add_argument('--test_folder', help='Folder to save test results', type=str, default='./test')
    parser.add_argument('--prefix', help='Prefix for model filename', type=str, default=None)
    parser.add_argument('--epochs', help='Epochs to train for', type=int, default=40)
    parser.add_argument('--dataset_test', help='Which dataset is used for testing', choices=["train", "val", "test"],
                        type=str, default="test")
    parser.add_argument('--lr', help='Learning rate', type=float, default=1e-3)
    parser.add_argument('--bce_weight', help='Cross Entropy weight the lower the higher the dice loss weight',
                        type=float, default=.5)
    parser.add_argument('--threshold_ndwi', help='NDWI threshold',
                        type=float, default=0.)
    parser.add_argument('--seed', help='Random number seed', type=int, default=1)
    parser.add_argument('--multigpu', help='Use multiple GPUs', action='store_true')
    parser.add_argument('--plotall', help='Plot probabilities and channels in test mode',
                        action='store_true')
    parser.add_argument('--weightloss', dest="weightloss",
                        help='Weight the loss of the multiclass classification loss '
                             'according to the observed frequency',
                        action='store_true')
    parser.add_argument("--no_weightloss", dest="weightloss", action="store_false",
                        help='Disable weight the loss of the multiclass classification loss '
                             'according to the observed frequency')
    parser.set_defaults(weightloss=True)
    parser.add_argument('--pbartrain', dest="pbartrain",
                        help='Show pbar for training',
                        action='store_true')
    parser.add_argument("--no_pbartrain", dest="pbartrain", action="store_false",
                        help='Hide pbar for training')
    parser.set_defaults(pbartrain=True)
    parser.add_argument('--folder_cache', help="path to folder where worldfloods cache data is stored", type=str,
                        required=False)
    parser.add_argument('--dataset_limit', help='Cutoff for dataset length', type=int, default=None)
    parser.add_argument('--window_size', help='Crop size for worldfloods e.g. 256', type=int, default=256)
    parser.add_argument('--max_tile_size',
                        help='Tile size to predict. If the patches yielt '
                             'by the dataset are bigger the pred will on tiles un max_tile_size',
                        type=int, default=1280)
    parser.add_argument('--downsampling_factor', help='Resample input image patches', type=float, default=1)
    parser.add_argument('--augment', help='Augment training images', action='store_true')
    parser.add_argument('--overwrite', help='Overwrite existing files', action='store_true')
    parser.add_argument('--s2_channels', help='Select which S2 channels to use', type=str,
                        choices=list(worldfloods_dataset.CHANNELS_CONFIGURATIONS.keys()), default='all')
    parser.add_argument('--compression', choices=["", "lzf", "gzip"], help="Compression of the hdf5 file "
                                                                           "for cache option",
                        default="")
    opt = parser.parse_args(args=args)

    return opt


def main():
    try:
        print(colored('FDL Europe 2019 Disaster Management', 'white', attrs=['bold']))
        opt = parse_args()

        print('Arguments:')

        pprint.pprint(vars(opt), depth=2, width=50)
        print()

        set_random_seed(opt.seed)

        if opt.mode == 'train':
            train(opt)
        elif opt.mode == 'test':
            test(opt)
        elif opt.mode == "cache":
            cache_data(opt)
        elif opt.mode == 'pr':
            precision_recall(opt)
        elif opt.mode == "trainleaveoneout":
            trainleaveoneout(opt)
        else:
            print('Unknown mode: {}'.format(opt.mode))
    except KeyboardInterrupt:
        print('Stopped.')
    except Exception:
        traceback.print_exc(file=sys.stdout)


if __name__ == '__main__':
    main()
