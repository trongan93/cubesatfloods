from data import worldfloods_dataset, WorldFloodsDataset
from data import worldfloods_dataset_cache
import numpy as np
from models.trasformations_dataset import get_augmentation_test, get_augmentation_train
import os
from glob import glob


def get_normalisation(use_channels):
    
    s2_channels = worldfloods_dataset.CHANNELS_CONFIGURATIONS[use_channels]

    s2_norm = worldfloods_dataset.SENTINEL2_NORMALIZATION[s2_channels]

    #  channel stats for now
    sentinel_means =s2_norm.copy()[:, 0]
    sentinel_means = sentinel_means[np.newaxis, np.newaxis]
    sentinel_std = s2_norm.copy()[:, 1]
    sentinel_std = sentinel_std[np.newaxis, np.newaxis]
    
    return sentinel_means, sentinel_std


CV_TEST_EXCLUDE = [
    ['EMSR286_08ITUANGONORTH_DEL_MONIT02_v1_observed_event_a.tif',
     'EMSR286_09ITUANGOSOUTH_DEL_MONIT02_v1_observed_event_a.tif'],
    ['EMSR333_01RATTALORO_DEL_MONIT01_v1_observed_event_a.tif',
     'EMSR333_02PORTOPALO_DEL_MONIT01_v1_observed_event_a.tif',
     'EMSR333_13TORRECOLONNASPERONE_DEL_MONIT01_v2_observed_event_a.tif'],
    ['EMSR342_06NORTHNORMANTON_DEL_v1_observed_event_a.tif',
     'EMSR342_07SOUTHNORMANTON_DEL_MONIT03_v2_observed_event_a.tif'],
    ['EMSR347_06MWANZA_DEL_v1_observed_event_a.tif',
     'EMSR347_07ZOMBA_DEL_MONIT01_v1_observed_event_a.tif',
     'EMSR347_07ZOMBA_DEL_v2_observed_event_a.tif'],
    ['EMSR9284_01YLITORNIONORTHERN_DEL_MONIT01_v1_observed_event_a.tif']
]


def get_worldfloods_data(width, subset, downsampling_factor=1, augment=False,
                         normalize=False,
                         worldfloods_root="../WORLDFLOODS",
                         dataset_limit=None,
                         use_channels='all',
                         folder_cache=None,
                         cv_index=None):

    assert subset in {"train", "test", "val"}, "Subset %s not recognized" % subset

    sentinel_means, sentinel_std = get_normalisation(use_channels)

    if subset == "train":
        transform = get_augmentation_train(sentinel_means, sentinel_std,
                                           input_size=width, downsampling_factor=downsampling_factor,
                                           augment=augment, normalize=normalize)
    else:
        transform = get_augmentation_test(sentinel_means, sentinel_std,
                                          input_size=width, downsampling_factor=downsampling_factor,
                                          normalize=normalize)

    # force testing always with big images
    if subset != "test" and (width > 0):
        window_size = (width, width)
    else:
        window_size = None

    # Load dataset
    if cv_index is None:
        print("Loading %s WORLDFLOODS dataset" % subset)
        if folder_cache is None:
            x_filenames = glob(os.path.join(worldfloods_root, subset, "S2", "*.tif"))
            assert len(x_filenames) > 0, "Not files found in %s" % os.path.join(worldfloods_root, subset, "S2")
            dataset = WorldFloodsDataset(x_filenames=x_filenames,
                                         sample=False,
                                         window_size=window_size,
                                         limit=dataset_limit,
                                         transform=transform,
                                         use_channels=use_channels)

        else:
            # WorldFloodsDatasetCached
            filename_train = os.path.join(folder_cache,
                                          worldfloods_dataset_cache.filename_cache(subset, window_size[0]))
            dataset = worldfloods_dataset_cache.WorldFloodsDatasetCached(filename=filename_train,
                                                                         window_size=window_size,
                                                                         limit=dataset_limit,
                                                                         transform=transform,
                                                                         use_channels=use_channels)

    else:
        # cv mode use only test images
        assert cv_index < len(CV_TEST_EXCLUDE), "cv index is greater than the number of possible cross validations"

        images_test = [os.path.join(worldfloods_root, "test", "S2", t) for t in CV_TEST_EXCLUDE[cv_index]]
        all_tests = set()
        for sub in CV_TEST_EXCLUDE:
            for v in sub:
                all_tests.add(os.path.join(worldfloods_root, "test", "S2", v))

        images_train = [img for img in all_tests if img not in images_test]

        if subset == "train":
            images = images_train
        else:
            images = images_test

        assert len(images) > 0, "Not files found in %s" % os.path.join(worldfloods_root, "test", "S2")

        print("CV mode loading %s %d images" % (subset, len(images)))

        dataset = WorldFloodsDataset(x_filenames=images,
                                     sample=False,
                                     window_size=window_size,
                                     limit=dataset_limit,
                                     transform=transform,
                                     use_channels=use_channels)

    return dataset
