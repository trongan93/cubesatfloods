import torch
from utils import plot_preds
import numpy as np
from tqdm import tqdm


KWARGS_PLOT_DEFAULT = {
    "plot_probabilities_and_channels": False,
    "normalized": True,
    "figsize": None,
}


def compute_confusions(ground_truth_outputs, test_outputs_categorical, num_class):

    confusions_batch = torch.zeros(size=(ground_truth_outputs.shape[0], num_class, num_class),
                                   dtype=torch.long)
    for c in range(num_class):
        gtc = (ground_truth_outputs == c)
        for c1 in range(num_class):
            pred_c1 = (test_outputs_categorical == c1)
            confusions_batch[:, c1, c] = (pred_c1 & gtc).sum(dim=(1, 2))

    return confusions_batch


def test_loop_thresholds(dataloader, pred_fun, thresholds_water=np.arange(0, 1, .05)):
    confusions = []

    # Sort thresholds from high to low
    thresholds_water = np.sort(thresholds_water)
    thresholds_water = thresholds_water[-1::-1]
    
    for i, (test_inputs, ground_truth_outputs) in tqdm(enumerate(dataloader)):
        test_outputs = pred_fun(test_inputs, ground_truth_outputs)

        test_outputs_categorical = torch.zeros(ground_truth_outputs.shape, dtype=torch.long,
                                               device=torch.device("cpu"))

        # Move all to cpu
        ground_truth_outputs = ground_truth_outputs.to(test_outputs_categorical.device)
        test_outputs = test_outputs.to(test_outputs_categorical.device)
        
        # Save invalids to discount
        invalids = ground_truth_outputs == 0
        ground_truth_outputs[invalids] = 1
        ground_truth_outputs -= 1

        # Set clouds to land
        ground_truth_outputs[ground_truth_outputs == 2] = 0

        results = []
        valids = ~invalids

        # thresholds_water sorted from high to low
        for threshold in thresholds_water:
            # keep invalids in pred to zero
            test_outputs_categorical[valids & (test_outputs[:, 1] > threshold)] = 1

            confusions_batch = compute_confusions(ground_truth_outputs,
                                                  test_outputs_categorical, num_class=2)   # [batch_size, 2, 2]

            # Discount invalids
            confusions_batch[:, 0, 0] -= torch.sum(invalids)

            results.append(confusions_batch.numpy())

        # results is [len(thresholds), batch_size, 2, 2]
        confusions.append(np.stack(results))

    confusions = np.concatenate(confusions, axis=1)

    return confusions, thresholds_water


def test_loop(dataloader, pred_fun_final, num_class, idx_show={},
              batch_size=32, kwargs_plot=None,
              filename_fun=lambda idx: "%s.jpg"):

    if kwargs_plot is None:
        kwargs_plot = KWARGS_PLOT_DEFAULT.copy()

    confusions = []
    for i, (test_inputs, ground_truth_outputs) in enumerate(dataloader):
        assert ground_truth_outputs.dim() == 3, "Expected dim output 3 (one value per category)"
        assert test_inputs.dim() == 4, "Expected dim input 4"

        test_outputs = pred_fun_final(test_inputs, ground_truth_outputs)

        assert test_outputs.dim() == 4, "Expected dim 4 for test outputs"
        assert test_outputs.shape[1] == num_class, "Expected %d classes found %d" % (num_class, test_outputs.shape[1])

        test_outputs_categorical = torch.argmax(test_outputs, dim=1).long()
        ground_truth_outputs = ground_truth_outputs.to(test_outputs_categorical.device)

        # Save invalids to discount
        invalids = ground_truth_outputs == 0  # (batch_size, H, W) gpu
        ground_truth_outputs[invalids] = 1
        ground_truth_outputs -= 1

        # Set invalids in pred to zero
        test_outputs_categorical[invalids] = 0  # (batch_size, H, W)

        confusions_batch = compute_confusions(ground_truth_outputs, test_outputs_categorical, num_class=num_class)
        # confusions_batch is (batch_size, num_class, num_class)

        # Discount invalids
        inv_substract = torch.sum(invalids, dim=(1, 2)).to(confusions_batch.device)

        confusions_batch[:, 0, 0] -= inv_substract
        confusions.extend(confusions_batch.tolist())

        # Call plot prediction
        for batch_element in range(ground_truth_outputs.shape[0]):
            idx_over_dataset = i*batch_size + batch_element
            if idx_over_dataset not in idx_show:
                continue

            file_name = filename_fun(idx_over_dataset)
            print('Saving {}: {}'.format(idx_over_dataset, file_name))
            plot_preds.plot_prediction(model_input=test_inputs[batch_element],
                                       model_output=test_outputs[batch_element],
                                       ground_truth_output_categorical=ground_truth_outputs[batch_element],
                                       file_name=file_name,
                                       num_ground_truth_categories=num_class,
                                       **kwargs_plot)

    return confusions
