from data import worldfloods_dataset
import h5py
from tqdm import tqdm
import os
from torch.utils.data import Dataset
import numpy as np


class WorldFloodsDatasetCached(Dataset):
    def __init__(self, window_size, filename, transform=None,
                 use_channels='all', limit=None, load_in_memory=False):

        self.filename = filename
        self.window_size = window_size

        assert os.path.exists(self.filename), "Cache file %s does not exists" % self.filename
        with h5py.File(self.filename, "r") as input_:
            assert input_.attrs["ALL_CACHED"], "Flag ALL_CACHED false. Error when generated cache"

        print("Using cache file: %s" % self.filename)
        self.load_in_memory = load_in_memory

        with h5py.File(self.filename, "r") as input_:
            all_cached = input_.attrs["ALL_CACHED"]
            assert all_cached, "Not all the data is cached. Generate again the cache file!"
            self._n_items = len(input_["X0"])
            assert input_["X0"].shape[2:] == window_size, "Different shapes than expected X0 {} {}".format(
                input_["X0"].shape[2:], window_size)
            assert input_["X1"].shape[1:] == window_size, "Different shapes than expected X1 {} {}".format(
                input_["X1"].shape[1:], window_size)

            if self.load_in_memory:
                print("Loading data in memory")
                self.input_ = dict()
                if limit is None:
                    self.input_["X0"] = input_["X0"][...]
                    self.input_["X1"] = input_["X1"][...]
                else:
                    _indexes = np.random.choice(self._n_items, size=limit, replace=False)
                    self._n_items = limit
                    self.input_["X0"] = np.zeros((limit, 13) + window_size, dtype=np.uint16)
                    self.input_["X1"] = np.zeros((limit,) + window_size, dtype=np.uint8)
                    for i, idx in enumerate(_indexes):
                        self.input_["X0"][i] = input_["X0"][idx:(idx+1), ...]
                        self.input_["X1"][i] = input_["X1"][idx:(idx+1), ...]
            else:
                if limit is None:
                    self._indexes = np.arange(self._n_items)
                else:
                    self._indexes = np.random.choice(self._n_items, size=limit, replace=False)
                    self._n_items = limit

        self.s2_channels = worldfloods_dataset.CHANNELS_CONFIGURATIONS[use_channels]
        self.transform = transform

    def __len__(self):
        return self._n_items

    def __getitem__(self, idx):
        if self.load_in_memory:
            x = self.input_["X0"][idx][self.s2_channels]
            y = self.input_["X1"][idx]
        else:
            idx_real = self._indexes[idx]
            with h5py.File(self.filename, "r") as input_:
                x = input_["X0"][idx_real][self.s2_channels]
                y = input_["X1"][idx_real]

        # Stored as {0: invalid, 1: land, 2: water, 3: cloud}

        if self.transform is not None:
            res = self.transform(image=x.transpose(1, 2, 0),
                                 mask=y)
            x, y = res['image'], res['mask']

        return x, y


def filename_cache(mode, width):
    return "%sv3_%d.hdf5" % (mode, width)


def cache_data(window_size, x_filenames, name_file_out, compression="lzf"):
    train_dataset = worldfloods_dataset.WorldFloodsDataset(x_filenames,
                                                           sample=False,
                                                           window_size=window_size,
                                                           transform=None,
                                                           use_channels="all")
    folder_cache = os.path.dirname(name_file_out)
    if not os.path.exists(folder_cache):
        os.mkdir(folder_cache)

    if os.path.exists(name_file_out):
        with h5py.File(name_file_out, mode="r") as input_:
            all_cached = input_.attrs["ALL_CACHED"]

        if all_cached:
            print("File %s already exists and all data is cached" % name_file_out)
            return
        os.remove(name_file_out)

    with h5py.File(name_file_out, mode="w") as output:
        output.attrs["ALL_CACHED"] = False
        len_ds = len(train_dataset)
        dsx = output.create_dataset("X0",
                                    shape=(len_ds,) + (len(worldfloods_dataset.BANDS_S2), )+window_size,
                                    dtype="uint16",
                                    chunks=(1, len(worldfloods_dataset.BANDS_S2),)+window_size,
                                    compression=compression)
        dsy = output.create_dataset("X1",
                                    shape=(len_ds,) + window_size,
                                    dtype="uint8",
                                    chunks=(1,)+window_size,
                                    compression=compression)
        output.create_dataset("x_filenames", data=np.array(train_dataset.x_filenames, dtype="S"))
        slices_array = []
        for slice_tuple in train_dataset.slices:
            slices_array.append([[s.start, s.stop] for s in slice_tuple])
        output.create_dataset("slices", data=np.array(slices_array))

        for i, (x, y) in tqdm(enumerate(train_dataset), total=len_ds):
            dsx[i:(i + 1), ...] = x
            dsy[i:(i + 1), ...] = y
        output.attrs["ALL_CACHED"] = True
