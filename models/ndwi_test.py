import numpy as np
from models.test import test_loop_thresholds, compute_confusions
from models.architectures import ndwi as ndwi_module
from data import worldfloods_dataset
from utils import plot_preds, process_results, fsutils
import torch
import json
import os


def IoU(cm):
    return cm[1, 1]/(cm[0, 1]+cm[1, 1]+cm[1, 0]+1e-6)


def recall(cm):
    true = np.sum(cm[:, 1])
    return cm[1, 1]/(true+1e-6)


MIN_RECALL_THRESHOLD = .94


def run_pr_curve_ndwi(filename_save, dataloader,
                      thresholds_water=np.arange(-1, 1, .1)):

    pred_fun_final = ndwi_module.ManualWaterBodyNDWI(to_probability=False)

    confusions, thresholds_water = test_loop_thresholds(dataloader, pred_fun_final, thresholds_water=thresholds_water)

    with open(filename_save, "w") as fh:
        json.dump({"confusion_matrices": [fsutils.castlistmatrix(x) for x in confusions],
                   "thresholds_water": fsutils.cast2(thresholds_water, float)},
                  fh)

    test_folder = os.path.dirname(filename_save)
    confusions_best_threshold = []
    filenames = []

    cms_add = np.sum(confusions, axis=1)  # (len(thresholds), 2, 2) matrix
    iou = np.array([IoU(c) if (recall(c) >= MIN_RECALL_THRESHOLD) else 0 for c in cms_add])
    assert len(iou) == len(thresholds_water), f"Different lens {len(iou)} {len(thresholds_water)}"

    best_idx_iou = np.argmax(iou)
    threshold_water = thresholds_water[best_idx_iou]
    print(f"Plotting results for best threshold {threshold_water:.2f}")

    pred_fun_final.threshold_water = threshold_water
    pred_fun_final.to_probability = True

    for i, (test_inputs, ground_truth_outputs) in enumerate(dataloader):
        assert ground_truth_outputs.dim() == 3, "Expected dim output 3 (one value per category)"
        assert test_inputs.dim() == 4, "Expected dim input 4"
        assert test_inputs.shape[0] == 1, "Expected batch_size to be 1 for the test set"

        # Find best NDWI threshold for each image
        # iou_current_i = np.array([IoU(c) for c in confusions[:, i]])
        # best_idx_iou = np.argmax(iou_current_i)
        # assert len(iou_current_i) == len(thresholds_water), f"Different lens {len(iou_current_i)} {len(thresholds_water)}"
        #  pred_fun_final.threshold_water = thresholds_water[best_idx_iou]

        test_outputs = pred_fun_final(test_inputs, ground_truth_outputs)

        # Save invalids to discount
        invalids = ground_truth_outputs == 0
        ground_truth_outputs[invalids] = 1
        ground_truth_outputs -= 1

        test_outputs_categorical = torch.argmax(test_outputs, dim=1).long()

        ground_truth_outputs = ground_truth_outputs.to(test_outputs_categorical.device)
        confusions_batch = compute_confusions(ground_truth_outputs, test_outputs_categorical, num_class=3)

        # Discount invalids
        confusions_batch[:, 0, 0] -= torch.sum(invalids)

        confusions_best_threshold.extend(confusions_batch.tolist())

        # Call plot prediction
        file_name = os.path.join(test_folder, f'worldfloods_test_{i}.jpg')
        kwargs_plot = {"plot_probabilities_and_channels": False,
                       "normalized": False,
                       "figsize": None,
                       "colors_cmap": worldfloods_dataset.COLORS_WORLDFLOODS}
        filenames.append(file_name)

        print('Saving {}: {}'.format(i, file_name))
        plot_preds.plot_prediction(model_input=test_inputs[0],
                                   model_output=test_outputs[0],
                                   ground_truth_output_categorical=ground_truth_outputs[0],
                                   file_name=file_name,
                                   num_ground_truth_categories=3,
                                   **kwargs_plot)

    name_html = os.path.join(test_folder, 'worldfloods_test.html')
    plot_preds.generate_html(list(range(len(dataloader.dataset))),
                             dataloader.dataset,
                             filenames,
                             name_html)

    confusions_best_threshold = np.array(confusions_best_threshold)

    fn_results = os.path.join(test_folder, "metrics_worldfloods.json")

    process_results.save_results(confusions_best_threshold,
                                 fn_results, num_class=3,
                                 extra={"model": f"NDWI_{threshold_water:.2f}"}, verbose=True)


